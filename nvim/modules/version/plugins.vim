
Plug 'tpope/vim-fugitive',{'on': [ 'G', 'Git', 'Gfetch', 'Gpush', 'Glog', 'Gclog', 'Gdiffsplit' ]}

Plug 'rhysd/committia.vim'

let g:committia_min_window_width = 7
