
Plug 'cakebaker/scss-syntax.vim' ,{'for' : [ 'scss', 'sass' ]}

Plug 'pangloss/vim-javascript', {'for': [ 'javascript', 'javascriptreact' ]}
Plug 'moll/vim-node', {'for': [ 'javascript', 'javascriptreact' ]}

Plug 'arzg/vim-sh', {'for': [ 'sh','zsh' ]}

Plug 'othree/html5.vim', {'for': 'html'}

Plug 'hail2u/vim-css3-syntax' ,{'for': 'css'}

Plug  'HerringtonDarkholme/yats.vim' , {'for' : [ 'typescript', 'typescriptreact' ]}

Plug  'dart-lang/dart-vim-plugin' ,{'for': 'dart'}

Plug 'posva/vim-vue' , {'for': 'vue'}

Plug 'tbastos/vim-lua',{'for': 'lua'}

