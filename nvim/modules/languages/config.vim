
call coc#config('languageserver', {
\ 'bash': {
	\ "command": "bash-language-server",
	\ "args" : ["start"],
	\ "ignoredRootPaths": ["~"],
	\ "filetypes": ["sh"]
	\ }
	\})

let g:html5_event_handler_attributes_complete = 0
let g:html5_rdfa_attributes_complete = 0
let g:html5_microdata_attributes_complete = 0
let g:html5_aria_attributes_complete = 0

call coc#add_extension('coc-html')

call coc#add_extension('coc-css')

call coc#add_extension('coc-flutter')

call coc#config ('languageserver', {
		\'lua': {
			\ "cwd": "full path of lua-language-server directory",
			\ "command": "full path to lua-language-server executable",
			\ "args": ["-E", "-e", "LANG=en", "[full path of lua-language-server directory]/main.lua"],
			\ "filetypes": ["lua"],
			\ "rootPatterns": [".git/"]
			\}
			\})
