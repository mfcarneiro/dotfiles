
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'honza/vim-snippets'
Plug 'tpope/vim-projectionist'
Plug 'liuchengxu/vista.vim'

" Dart
Plug 'natebosch/dartlang-snippets'
Plug 'natebosch/vim-lsc'
Plug 'natebosch/vim-lsc-dart'
