Plug 'ryanoasis/vim-devicons'


Plug 'dracula/vim'

Plug 'ayu-theme/ayu-vim'

Plug 'arcticicestudio/nord-vim'


Plug 'hardcoreplayers/dashboard-nvim'

Plug 'hardcoreplayers/vim-buffet'

Plug 'hardcoreplayers/spaceline.vim'

Plug 'preservim/nerdtree'
Plug 'liuchengxu/nerdtree-dash'
Plug 'Xuyuanp/nerdtree-git-plugin'
