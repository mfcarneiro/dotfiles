
Plug 'Shougo/context_filetype.vim'

Plug 'plasticboy/vim-markdown', {'for': 'markdown'}
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install','for':'markdown' }

Plug 'kevinoid/vim-jsonc',{'for': ['json','jsonc']}

Plug 'ekalinin/Dockerfile.vim',{'for':['Dockerfile','docker-compose']}
