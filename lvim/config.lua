vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.relativenumber = true
lvim.transparent_window = false
lvim.colorscheme = 'nightfox'
lvim.builtin.dap.active = true

-- general
lvim.log.level = "info"
lvim.format_on_save = {
  enabled = true,
  pattern = {
    "*.lua",
    "*.vue",
    "*.exs",
    "*.ex",
    "*.go",
    "*.ts",
    "*.js",
    "*.json",
    "*.md",
    "*.css",
    "*.scss",
    "*.sass",
    "*.kt",
    "*.kts",
    "*.dart"
  },
  timeout = 1000,
}
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

lvim.leader = ","
lvim.keys.normal_mode = {
  ["<C-s>"] = ":w<cr>",
  ["<Tab>"] = ":bnext<CR>",
  ["<S-Tab>"] = ":bprevious<CR>",
  ["<leader>gg"] = ":LazyGit<CR>"
}

-- vim.cmd("nnoremap <silent> <leader>gg :LazyGit<CR>")

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  {
    exe = "prettier",
    filetypes = { "typescript", "vue", "javascript", "json", "markdown", "css", "scss", "html", "sh" },
  },
}

local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults.mappings = {
  -- for input mode
  i = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
    ["<C-n>"] = actions.cycle_history_next,
    ["<C-p>"] = actions.cycle_history_prev,
  },
  -- for normal mode
  n = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
  },
}

lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true
lvim.builtin.treesitter.auto_install = false
lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.rainbow.enable = true -- Automatically install missing parsers when entering buffer
lvim.builtin.treesitter.auto_install = true

lvim.plugins = {
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  { "wakatime/vim-wakatime" },
  { "SirVer/Ultisnips" },
  { "tpope/vim-fugitive" },
  { "kdheepak/lazygit.nvim" },
  { "folke/tokyonight.nvim" },
  { "whatyouhide/vim-gotham" },
  { "EdenEast/nightfox.nvim" },
  { "nvim-neotest/neotest" },
  { "jfpedroza/neotest-elixir" },
  { "carlosgaldino/elixir-snippets" },
  { "brendalf/mix.nvim" },
  { "mattn/emmet-vim" },
  { "mfussenegger/nvim-jdtls" },
  { "stevearc/dressing.nvim" },
  { "akinsho/flutter-tools.nvim" },
  { "sidlatau/neotest-dart" },
  { "RobertBrunhage/flutter-riverpod-snippets" },
  { "Neevash/awesome-flutter-snippets" },
  { "lewis6991/gitsigns.nvim" },
  {
    "rmagatti/goto-preview",
    config = function()
      require('goto-preview').setup {
        width = 120,              -- Width of the floating window
        height = 25,              -- Height of the floating window
        default_mappings = false, -- Bind default mappings
        debug = false,            -- Print debug information
        opacity = 60,             -- 0-100 opacity level of the floating window where 100 is fully transparent.
        post_open_hook = nil,     -- A function taking two arguments, a buffer and a window to be ran as a hook.
        -- You can use "default_mappings = true" setup option
        -- Or explicitly set keybindings
        vim.cmd("nnoremap gpd <cmd>lua require('goto-preview').goto_preview_definition()<CR>"),
        vim.cmd("nnoremap gpi <cmd>lua require('goto-preview').goto_preview_implementation()<CR>"),
        vim.cmd("nnoremap gP <cmd>lua require('goto-preview').close_all_win()<CR>"),
      }
    end
  },
  {
    "iamcco/markdown-preview.nvim",
    run = "cd app && npm install",
    ft = "markdown",
    config = function()
      vim.g.mkdp_auto_start = 1
    end,
  },
}

-- Elixir
require("mix").setup()

-- Flutter
require("flutter-tools").setup {
  ui = {
    border = "rounded",
    notification_style = 'native',
  },
  decorations = {
    statusline = {
      app_version = true,
      device = true,
    }
  },
  flutter_lookup_cmd = "asdf where flutter",
  closing_tags = {
    highlight = "", -- highlight for the closing tag
    prefix = "// ", -- character to use for close tag e.g. > Widget
    enabled = true  -- set to false to disable
  },
  dev_log = {
    enabled = false,
    open_cmd = "tabedit", -- command to use to open the log buffer
  },
  widget_guides = {
    enabled = true,
  },
  dev_tools = {
    autostart = true,         -- autostart devtools server if not detected
    auto_open_browser = true, -- Automatically opens devtools in the browser
  },
  outline = {
    open_cmd = "40vnew", -- command to use to open the outline buffer
    auto_open = true     -- if true this will open the outline automatically when it is first populated
  },
  settings = {
    showTodos = true,
    completeFunctionCalls = true,
    renameFilesWithClasses = "prompt",
    enableSnippets = true,
  }
}

require 'luasnip'.filetype_extend("dart", { "flutter" })
require("telescope").load_extension("flutter")

--Neotest
require("neotest").setup({
  adapters = {
    require("neotest-elixir"),
    require("neotest-dart"),
  },
})

-- Autocommands (`:help autocmd`) <https://neovim.io/doc/user/autocmd.html>
vim.api.nvim_create_autocmd("FileType", {
  pattern = "zsh",
  callback = function()
    -- let treesitter use bash highlight for zsh files as well
    require("nvim-treesitter.highlight").attach(0, "bash")
  end,
})
