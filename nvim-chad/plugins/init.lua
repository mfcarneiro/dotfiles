return {
  ["jose-elias-alvarez/null-ls.nvim"] = {
    after = "nvim-lspconfig",
    config = function()
      require("custom.plugins.null-ls").setup()
    end,
  },

  ["nvim-telescope/telescope-media-files.nvim"] = {
    after = "telescope.nvim",
    config = function()
      require("telescope").load_extension "media_files"
    end,
  },

  -- ["akinsho/flutter-tools.nvim"] = {
  --   after = "nvim-lspconfig",
  --   config = function()
  --     require("flutter-tools").setup()
  --   end,
  -- },

  ["dart-lang/dart-vim-plugin"] = {
    after = "nvim-lspconfig",
  },

  ["natebosch/dartlang-snippets"] = {
    after = "nvim-lspconfig",
  },

  ["natebosch/vim-lsc"] = {
    after = "nvim-lspconfig",
  },

  ["natebosch/vim-lsc-dart"] = {
    after = "nvim-lspconfig",
  },

  ["thosakwe/vim-flutter"] = {
    after = "nvim-lspconfig",
  }
}
