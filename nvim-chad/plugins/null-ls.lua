local null_ls = require "null-ls"
local b = null_ls.builtins

local sources = {

  -- webdev stuff
  b.formatting.prettier.with { filetypes = { "html", "markdown", "css", "tsscript", "javascript", "markdown" } },

  -- Shell
  b.diagnostics.shellcheck.with { diagnostics_format = "#{m} [#{c}]" },

  -- cpp
  b.formatting.clang_format,


  -- Lua
  b.formatting.lua_format,
  b.diagnostics.luacheck.with { extra_args = { "--global vim" } },
  b.completion.luasnip,

  -- Dart
  b.formatting.dart_format,
}

local M = {}

M.setup = function()
   null_ls.setup {
      debug = true,
      sources = sources,

      -- format on save
      on_attach = function(client)
         if client.resolved_capabilities.document_formatting then
            vim.cmd "autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()"
         end
      end,
   }
    end

return M
