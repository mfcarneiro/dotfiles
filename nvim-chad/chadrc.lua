local M = {}

local override = require "custom.plugins.override"
local userPlugins = require "custom.plugins"

M.ui = {
  theme = "nord",
}

M.options = {
  user = function()
    vim.opt.relativenumber = true
    vim.g.mapleader = ","
  end,

}

M.plugins = {
  options = {
    lspconfig = {
      setup_lspconf = "custom.plugins.lspconfig",
    },

    statusline = {
      separator_style = "round",
    },
  },

  user = userPlugins,

  override = {
    ["nvim-treesitter/nvim-treesitter"] = override.treesitter,
    ["nvim-telescope/telescope.nvim"] = override.telescope,
  },
}

-- M.mappings = require "custom.mappings"

return M
